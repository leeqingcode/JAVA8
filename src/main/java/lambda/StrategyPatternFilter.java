package lambda;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 策略模式的方式解决筛选问题
 */
public class StrategyPatternFilter {

    List<Employee> employeeList = new ArrayList<>();

    public void initEmployee(){
        Employee employee;
        for (int i=1;i<=5;i++){
            double salary = Math.random()*10000;
            BigDecimal bigDecimal = new BigDecimal(salary);
            salary =  bigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
            employee = new Employee((int) (100*Math.random()),"a"+i,salary);
            employee.setStatus(randomEnum(Employee.Status.class));
            employeeList.add(employee);
        }
        for (Employee employee1 : employeeList){
            System.out.println(employee1);
        }
    }

    //获得随机的枚举值
    public <T extends Enum<T>> T randomEnum(Class<T> clazz){
        return random(clazz.getEnumConstants());
    }

    public static <T> T random(T[] values){
        Random rand = new Random();
        return values[rand.nextInt(values.length)];
    }


    public List<Employee> filterEmployee(List<Employee> employees,MyPredicate<Employee> predicate){
        List<Employee> filterEmployees = new ArrayList<>();
        for (Employee employee : employees){
            if(predicate.filterProdicate(employee)){
                filterEmployees.add(employee);
            }
        }
        return filterEmployees;
    }

    @Test
    public void client(){
        StrategyPatternFilter strategyPattern = new StrategyPatternFilter();
        strategyPattern.initEmployee();
        List<Employee> filterList = strategyPattern.filterEmployee(strategyPattern.employeeList, new MyPredicate<Employee>() {
            /**
             * 筛选出年龄大于20的员工，采用策略模式
             */
            @Override
            public boolean filterProdicate(Employee employee) {
                if(employee.getAge()>20)
                    return true;
                return false;
            }
        });

        System.out.println("=========================================================");
        System.out.println("选出年龄大于20的员工");
        System.out.println("=========================================================");
        for (Employee employee : filterList){
            System.out.println(employee);
        }

        System.out.println("=========================================================");
        System.out.println("选出年龄大于30,并且工资大于5000的员工");
        System.out.println("=========================================================");
        filterList = strategyPattern.filterEmployee(strategyPattern.employeeList, new MyPredicate<Employee>() {
            /**
             * 筛选出年龄大于20的员工，采用策略模式
             */
            @Override
            public boolean filterProdicate(Employee employee) {
                if(employee.getAge()>20){
                    if(employee.getSalary()>5000){
                        return true;
                    }
                }
                return false;
            }
        });

        for (Employee employee : filterList){
            System.out.println(employee);
        }
    }
}

interface MyPredicate<T>{
    boolean filterProdicate(T t);
}


class Employee{
    private Integer age;
    private String name;
    private Double salary;
    private Status status;

    public Employee() {
    }

    public Employee(Integer age, String name, Double salary) {
        this.age = age;
        this.name = name;
        this.salary = salary;
    }

    public Employee(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", status=" + status +
                '}';
    }

    public enum Status{
        BUSY,
        FREE,
        VOCATION
    }

}

