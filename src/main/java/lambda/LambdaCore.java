package lambda;

import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Java8内置的四大核心函数式接口
 *
 * Consumer<T>:   消费型接口
 *                void accept(T t)
 *
 * Supplier<T>:   供给型接口
 *                T get()
 *
 * Function<T,R>: 函数型接口
 *                R apply(T t)
 *
 * Predicate<T>:  断言型接口
 *                boolean test(T t)
 */
public class LambdaCore {

    @Test
    public void client(){
        Consumer<String> consumer = x-> System.out.println(x);
        consumer.accept("你好");

        Supplier<Integer> supplier = ()-> (int)(Math.random()*100);
        System.out.println(supplier.get());

        Function<String,String> function = (x) -> x.substring(6);
        System.out.println(function.apply("hello lambda"));

        Predicate<Integer> predicate = (x) -> x>10;
        System.out.println(predicate.test(9));
    }


}
