package lambda;

import org.junit.Test;

/**
 * 使用Stream API的方式筛选
 */
public class StreamFilter {
    @Test
    public void client(){
        StrategyPatternFilter strategyPattern = new StrategyPatternFilter();
        strategyPattern.initEmployee();
        System.out.println("=========================================================");
        System.out.println("选出年龄大于30的员工");
        System.out.println("=========================================================");
        strategyPattern.employeeList.stream().filter((e)->e.getAge()>30).forEach(System.out::println);
    }
}
