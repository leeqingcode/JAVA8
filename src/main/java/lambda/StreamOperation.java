package lambda;

import org.junit.Test;

import java.util.stream.Stream;

/**
 * Lambda的中间操作
 * 筛选和切片
 * filter -- 从流中排除某些元素
 * limit  -- 使其元素不超过指定数量
 * skip   -- 跳过元素，返回了一个跳过了n个元素的流，若元素不足n,则返回一个空流。
 * distinct -- 筛选，通过流中元素的hashcode()和equals()去除重复元素，故需要重写这两个方法
 *
 * 映射
 * map -- 将元素转成其他形式或提取指定数据，接受一个函数作为参数，该函数会被应用到每一个元素，并将其映射成新的元素
 * flatMap -- 接受一个函数作为参数，将流中的每一个值都换成另一个流，然后把所有的流都合成一个流，类似于addAll方法
 *
 * 排序
 * sorted  -- 自然排序(Comparable)
 * sorted(Comparator) -- 定制排序
 */

public class StreamOperation {

    //筛选，中间操作不会执行任何的操作，只有遇见终止操作才会一次性执行前面的所有操作
    @Test
    public void test(){
        StrategyPatternFilter strategyPatternFilter = new StrategyPatternFilter();
        strategyPatternFilter.initEmployee();
        System.out.println("===================================================================");
        Stream stream = strategyPatternFilter.employeeList.stream().filter(e->{
            //以下操作在没有遇见终止操作前不会执行，这个特性叫做“惰性求值”
            System.out.println("执行中间操作");
            return e.getAge()>30;
        }).skip(1).limit(2);
        //内部迭代
        stream.forEach(x-> System.out.println(x));
    }

    //map
    @Test
    public void test_01(){
        StrategyPatternFilter strategyPatternFilter = new StrategyPatternFilter();
        strategyPatternFilter.initEmployee();
        System.out.println("======================================================================");
        Stream<Employee> stream = strategyPatternFilter.employeeList.stream().map(x -> {
            x.setSalary(x.getSalary()+1000);
            return x;
        });
        stream.forEach(System.out::println);
    }

    //flatMap
    @Test
    public void test_02(){
        StrategyPatternFilter strategyPatternFilter = new StrategyPatternFilter();
        strategyPatternFilter.initEmployee();
        System.out.println("======================================================================");
        Stream<Employee> stream = strategyPatternFilter.employeeList.stream().flatMap(x -> {
            x.setSalary(x.getSalary()+1000);
            return Stream.of(x);//Stream<Employee>
        });
        stream.forEach(System.out::println);
    }

    //定制排序
    @Test
    public void test_03(){
        StrategyPatternFilter strategyPatternFilter = new StrategyPatternFilter();
        strategyPatternFilter.initEmployee();
        System.out.println("======================================================================");
        strategyPatternFilter.employeeList.stream().sorted((e1,e2) -> {
            if(e1.getAge().equals(e2.getAge())){
                return e1.getSalary().compareTo(e2.getSalary());
            }else{
                return e1.getAge().compareTo(e2.getAge());
            }
        }).forEach(System.out::println);
    }
}
