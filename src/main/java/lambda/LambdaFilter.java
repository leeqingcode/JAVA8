package lambda;

import org.junit.Test;

import java.util.List;

/**
 * 使用Lambda的方式筛选
 */
public class LambdaFilter {

    @Test
    public void client(){
        StrategyPatternFilter strategyPattern = new StrategyPatternFilter();
        strategyPattern.initEmployee();
        List<Employee> filterList = strategyPattern.filterEmployee(strategyPattern.employeeList,(e) -> e.getAge()>30);
        System.out.println("=========================================================");
        System.out.println("选出年龄大于30的员工");
        System.out.println("=========================================================");
        for (Employee employee : filterList){
            System.out.println(employee);
        }
    }

}

