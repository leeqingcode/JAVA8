package lambda;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Java8 Stream API
 *
 * Stream API 的三个步骤
 *
 * 创建流
 *
 * 中间操作
 *
 * 终止操作（终端操作）
 */
public class StreamCreateDemo {

    @Test
    public void test(){
        //通过集合的stream()方法创建流
        List<String> list = new ArrayList<>();
        Stream<String> stream =  list.stream();

        //通过Arrays中的静态方法stream()创建流
        Employee[] emps = new Employee[10];
        Stream<Employee> stream1 = Arrays.stream(emps);

        //通过Stream类的静态方法创建流
        Stream<String> stringStream = Stream.of("aa","bb","cc");

        //创建无限流
        //迭代
        Stream.iterate(0,x->x+2).limit(10).forEach(System.out::println);

        //生成
        Stream.generate(()->Math.random()).limit(10).forEach(System.out::println);

    }
}
