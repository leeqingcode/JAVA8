package lambda;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Lambda表达式基础：
 *
 * 语法格式一：无参数，无返回值
 *              （）->System.out.println("Hello Lamdba" );
 *
 * 语法格式二：有一个参数，无返回值
 *              （x）->System.out.println(x);
 *
 * 语法格式三：如果只有一个参数，括号可以不写
 *
 *
 * 语法格式四：有两个参数，有返回值
 *              （x，y）->{
 *                  return Integer.compare(x,y);
 *              };
 *
 * 语法格式五：有两个参数，有返回值，并且方法只有一条语句，括号和retuen都可以不写
 *
 *
 * 语法格式六：表达式的参数列表的参数类型可以不写，类型推断
 *
 *
 * 函数式接口：接口中只有一个方法的接口，@FunctionalInterface用于检查函数式接口，编译检查
 */

public class LambdaBasic {

    @Test
    public void client(){
        func_2();
    }

    public void func_1(){
        //匿名内部类中的变量需要是final,在java8中自动加上final
        int num = 10;
        Runnable runnable = ()-> System.out.println("Hello Lamdba" + num);
        runnable.run();
        //num = 1;
    }

    public void func_2(){

        List<Employee> employees = this.getEmployeeList();

        Collections.sort(employees,(x,y)->{
            if(x.getAge()==y.getAge()){
                //薪资高的在前
                return -x.getSalary().compareTo(y.getSalary());
            }else{
                //年龄小的在前
                return x.getAge().compareTo(y.getAge());
            }
        });
        System.out.println("================================================");
        for (Employee employee : employees)
            System.out.println(employee);
    }

    public List<Employee> getEmployeeList(){
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(21,"a",1000.0));
        employees.add(new Employee(21,"b",6000.0));
        employees.add(new Employee(38,"c",7000.0));
        employees.add(new Employee(65,"d",8900.0));
        employees.add(new Employee(42,"e",1800.0));
        return employees;
    }
}
