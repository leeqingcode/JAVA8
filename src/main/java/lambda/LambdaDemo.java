package lambda;

import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Lambda表达式例子
 */
public class LambdaDemo {
    //不使用Lambda
    public static void main(String[] args) {

        Comparator comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return -o1.compareTo(o2);
            }
        };

        TreeSet<Integer> treeSet = new TreeSet<Integer>(comparator);

        for (int i = 0; i<10; i++){
            treeSet.add(i);
        }


        Iterator<Integer> itr = treeSet.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }

    //Lambda表达式
    @Test
    public void test(){
        Comparator<Integer> comparator = (x,y) -> Integer.compare(x,y);
        TreeSet<Integer> treeSet = new TreeSet<Integer>(comparator);
        for (int i = 0; i<10; i++){
            treeSet.add(i);
        }
        Iterator<Integer> itr = treeSet.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }

}
