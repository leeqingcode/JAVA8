package lambda;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Stream API 的终止操作
 * 查找于匹配
 * allMatch -- 检查是否匹配所有元素
 * anyMatch -- 检查是否至少匹配一个元素
 * noneMatch -- 检查是否没有匹配所有元素
 * findFirst -- 返回第一个元素
 * findAny -- 返回当前流中的任意一个元素
 * count -- 返回当前流的个数
 * max -- 返回最大值
 * min -- 返回最小值
 */
public class StreamTermination {

    @Test
    public void test(){
        StrategyPatternFilter strategyPatternFilter = new StrategyPatternFilter();
        strategyPatternFilter.initEmployee();
        List<Boolean> list = new ArrayList<>();
        System.out.println("==========================================");
        list.add(strategyPatternFilter.employeeList.stream().allMatch(x->x.getStatus().equals(Employee.Status.BUSY)));
        list.add(strategyPatternFilter.employeeList.stream().anyMatch(x->x.getStatus().equals(Employee.Status.BUSY)));
        list.add(strategyPatternFilter.employeeList.stream().noneMatch(x->x.getStatus().equals(Employee.Status.BUSY)));
        list.forEach(System.out::println);
        Optional<Employee> op = strategyPatternFilter.employeeList.stream().findFirst();
        System.out.println("第一个元素"+op.get());
        Optional<Employee> op2 = strategyPatternFilter.employeeList.stream().filter(x->x.getStatus().equals(Employee.Status.BUSY)).findAny();
        System.out.println("任意元素:"+op2.get());
        System.out.println("元素个数:"+strategyPatternFilter.employeeList.stream().count());
        System.out.println("最大值:"+strategyPatternFilter.employeeList.stream().max(Comparator.comparing(Employee::getSalary)).get());
        System.out.println("最小值:"+strategyPatternFilter.employeeList.stream().min(Comparator.comparing(Employee::getSalary)).get());
    }
}
