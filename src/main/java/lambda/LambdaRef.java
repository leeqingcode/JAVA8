package lambda;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 方法引用：一种更加简便的形式，如果Lambda体中的内容已经实现，我们可以使用“方法引用”
 *
 * 主要有三种语法格式：
 *
 * 对象::实例方法
 *
 * 类::静态方法
 *
 * 类::实例方法
 *
 * 其他：构造器引用，数组引用
 *
 */
public class LambdaRef {

    //对象::实例的方法
    @Test
    public void test(){
        Consumer<String> consumer = (x) -> System.out.println(x);
        consumer.accept("hello lambda");

        //方法引用的方式变得更加的简单
        //要求:引用的方法的参数列表和返回值类型要和抽象法法中的参数列表和返回值类型保持一致
        Consumer<String> consumerRef = System.out::println;
        consumerRef.accept("你好 Lambda");

    }

    //类::静态方法名
    @Test
    public void test_02(){
        Comparator<Integer> comparator = (x,y) -> Integer.compare(x,y);

        //方法引用的方式
        Comparator<Integer> comparatorRef = Integer::compare;
        System.out.println(comparatorRef.compare(1,5));
    }

    //类::实例方法名
    @Test
    public void test_03(){
        BiPredicate biPredicate = (x,y) -> x.equals(y);

        //方法引用的方式
        BiPredicate<String,String> biPredicateRef = String::equals;
        System.out.println(biPredicateRef.test("a","b"));
    }

    //构造器引用
    //注意:需要调用的构造器的参数列表需要和接口中抽象方法的参数列表保持一致
    @Test
    public void test_04(){
        Supplier<Employee> supplier = ()->new Employee(21,"lee",1000.0);

        //构造器引用的方式
        Supplier<Employee> supplierRef = Employee::new;
        System.out.println(supplierRef.get());

        System.out.println("=====================================================================");
        Function<Integer,Employee> function = (x)->new Employee(x);
        Function<Integer,Employee> functionRef = Employee::new;
        System.out.println(functionRef.apply(21));
    }

    //数组引用
    @Test
    public void test_05(){
        Function<Integer,String[]> function = (x) -> new String[x];
        //数组引用的方式
        Function<Integer,String[]> functionRef = String[]::new;
        System.out.println(functionRef.apply(2).length);
    }

}
